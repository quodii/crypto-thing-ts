#!/usr/bin/env bash

BUILD_DIR=${BUILD_DIR:-$PWD/build}
BUILD_VERSION=${BUILD_VERSION:-dev}
BUILD_FILE=${BUILD_FILE:-crypto-thing-$BUILD_VERSION.tar.gz}

echo "---> Cleanup"
mkdir -p ${BUILD_DIR}
rm -rf ${BUILD_DIR}/*

echo "---> Creating Artifact (${BUILD_DIR}/${BUILD_FILE})"

echo "---> Install Dependencies"
npm install;

echo "---> Compile"
npm run build;

echo "---> Install Dependencies"
npm prune --production;

echo "---> Package"
tar -zcf ${BUILD_DIR}/${BUILD_FILE} \
 --exclude="${BUILD_DIR}/${BUILD_FILE}" \
 --exclude='*.git*' \
 --exclude='build*' \
 --exclude='./src*' \
 --exclude='tmp*' \
 --exclude='test*' \
 --exclude='ENV' \
 --exclude='scripts*' \
 --exclude='*.tar.gz' \
 .;

echo "---> Created ${BUILD_DIR}/${BUILD_FILE} artifact"
