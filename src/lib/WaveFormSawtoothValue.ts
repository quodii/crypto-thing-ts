import Logger from './Logger'
import { PoolingSensorValue } from 'quodii-lib'
import WaveFormBaseValue from './WaveFormBaseValue'

export default class WaveFormSawtoothValue extends WaveFormBaseValue {
  protected amplitude: number

  constructor(poolInterval: number, amplitude: number = 1, step: number = 1, start: number = 0) {
    super(poolInterval, step, start)

    this.amplitude = amplitude
    this.logger = new Logger('WaveFormSawtoothValue')
  }

  /**
   * Mimic an actual sensor updating its reading every couple seconds.
   */
  protected read(): Promise<number> {
    return super.read().then((value: number) => {
      if (value > this.amplitude) {
        this.counter = value = this.amplitude * -1
      }
      this.logger.debug(value)
      return value
    })
  }
}
