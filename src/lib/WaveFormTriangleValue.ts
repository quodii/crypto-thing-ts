import { PoolingSensorValue } from 'quodii-lib'
import WaveFormBaseValue from './WaveFormBaseValue'

export default class WaveFormTriangleValue extends WaveFormBaseValue {
  protected amplitude: number

  constructor(poolInterval: number, amplitude: number = 1, step: number = 1, start: number = 0) {
    super(poolInterval, step, start)

    this.amplitude = amplitude
  }

  /**
   * Mimic an actual sensor updating its reading every couple seconds.
   */
  protected read(): Promise<number> {
    return super.read().then((value: number) => {
      if (Math.abs(value) >= this.amplitude) {
        this.step = this.step * -1
      }

      return value
    })
  }
}
