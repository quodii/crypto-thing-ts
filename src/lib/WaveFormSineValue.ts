import { PoolingSensorValue } from 'quodii-lib'
import WaveFormBaseValue from './WaveFormBaseValue'

export default class WaveFormSineValue extends WaveFormBaseValue {
  protected amplitude: number

  constructor(poolInterval: number, amplitude: number = 1, step: number = 1, start: number = 0) {
    super(poolInterval, step, start)

    this.amplitude = amplitude
  }

  /**
   * Mimic an actual sensor updating its reading every couple seconds.
   */
  protected read(): Promise<number> {
    return super.read()
    .then((value: number) => (Math.sin(value * 0.017453301) * this.amplitude))
  }
}
