import debug from 'debug'

export default class Logger {
  public debug: debug
  public info: debug
  public warn: debug
  public error: debug
  public log: debug

  constructor(name: string) {
    this.debug = debug(`${name}:debug`)
    this.info = debug(`${name}:info`)
    this.warn = debug(`${name}:warn`)
    this.error = debug(`${name}:error`)
    this.log = this.debug
  }
}
