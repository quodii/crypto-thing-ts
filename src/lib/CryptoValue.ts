import { EventEmitterSensorValue } from 'quodii-lib'
import CryptoDevice from './CryptoDevice'

export default class CryptoValue extends EventEmitterSensorValue {
  public symbol: string
  public currency: string

  constructor(device: CryptoDevice, symbol: string, currency: string = 'USD') {
    super(device)

    this.symbol = symbol
    this.currency = currency
  }

  /**
   * Mimic an actual sensor updating its reading every couple seconds.
   */
  protected map(data: any): any {
    return data[this.symbol][this.currency]
  }
}
