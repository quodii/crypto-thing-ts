import Logger from './Logger';
import { PoolingSensorValue } from 'quodii-lib';
export default abstract class WaveFormBaseValue extends PoolingSensorValue {
    protected counter: number;
    protected start: number;
    protected step: number;
    protected logger: Logger;
    constructor(poolInterval: number, step?: number, start?: number);
    /**
     * Mimic an actual sensor updating its reading every couple seconds.
     */
    protected read(): Promise<number>;
}
