"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const quodii_lib_1 = require("quodii-lib");
const axios_1 = require("axios");
const Logger_1 = require("./Logger");
class CryptoDevice extends quodii_lib_1.PoolingSensorValue {
    constructor(symbols, currencies = ['USD'], poolInterval = 10000) {
        super(poolInterval);
        this.logger = new Logger_1.default('CryptoDevice');
        this.url = `https://min-api.cryptocompare.com/data/pricemulti?fsyms=${symbols.join(',')}&tsyms=${currencies.join(',')}`;
    }
    /**
     * Mimic an actual sensor updating its reading every couple seconds.
     */
    read() {
        return Promise.resolve(this.url)
            .then((url) => axios_1.default.get(url))
            .then((response) => response.data)
            .catch(this.logger.error);
    }
}
exports.default = CryptoDevice;
//# sourceMappingURL=CryptoDevice.js.map