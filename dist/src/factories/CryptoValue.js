"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const CryptoValue_1 = __importDefault(require("../lib/CryptoValue"));
const assert_1 = __importDefault(require("assert"));
module.exports = () => {
    return (data) => {
        const { device = null, symbol = null, currency = 'USD' } = data || {};
        assert_1.default(device, 'CryptoValue Error: "device" should be defined');
        assert_1.default(symbol, 'CryptoValue Error: "symbol" should be defined');
        return new CryptoValue_1.default(device, symbol, currency);
    };
};
//# sourceMappingURL=CryptoValue.js.map