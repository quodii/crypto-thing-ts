"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const debug_1 = __importDefault(require("debug"));
class Logger {
    constructor(name) {
        this.debug = debug_1.default(`${name}:debug`);
        this.info = debug_1.default(`${name}:info`);
        this.warn = debug_1.default(`${name}:warn`);
        this.error = debug_1.default(`${name}:error`);
        this.log = this.debug;
    }
}
exports.default = Logger;
//# sourceMappingURL=Logger.js.map