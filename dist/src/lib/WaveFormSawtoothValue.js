"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Logger_1 = __importDefault(require("./Logger"));
const WaveFormBaseValue_1 = __importDefault(require("./WaveFormBaseValue"));
class WaveFormSawtoothValue extends WaveFormBaseValue_1.default {
    constructor(poolInterval, amplitude = 1, step = 1, start = 0) {
        super(poolInterval, step, start);
        this.amplitude = amplitude;
        this.logger = new Logger_1.default('WaveFormSawtoothValue');
    }
    /**
     * Mimic an actual sensor updating its reading every couple seconds.
     */
    read() {
        return super.read().then((value) => {
            if (value > this.amplitude) {
                this.counter = value = this.amplitude * -1;
            }
            this.logger.debug(value);
            return value;
        });
    }
}
exports.default = WaveFormSawtoothValue;
//# sourceMappingURL=WaveFormSawtoothValue.js.map