"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const WaveFormBaseValue_1 = __importDefault(require("./WaveFormBaseValue"));
class WaveFormTriangleValue extends WaveFormBaseValue_1.default {
    constructor(poolInterval, amplitude = 1, step = 1, start = 0) {
        super(poolInterval, step, start);
        this.amplitude = amplitude;
    }
    /**
     * Mimic an actual sensor updating its reading every couple seconds.
     */
    read() {
        return super.read().then((value) => {
            if (Math.abs(value) >= this.amplitude) {
                this.step = this.step * -1;
            }
            return value;
        });
    }
}
exports.default = WaveFormTriangleValue;
//# sourceMappingURL=WaveFormTriangleValue.js.map