"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const WaveFormSawtoothValue_1 = __importDefault(require("./WaveFormSawtoothValue"));
class WaveFormSquareValue extends WaveFormSawtoothValue_1.default {
    /**
     * Mimic an actual sensor updating its reading every couple seconds.
     */
    read() {
        return super.read().then((value) => {
            if (value < 0) {
                value = this.amplitude * -1;
            }
            else {
                value = this.amplitude;
            }
            return value;
        });
    }
}
exports.default = WaveFormSquareValue;
//# sourceMappingURL=WaveFormSquareValue.js.map