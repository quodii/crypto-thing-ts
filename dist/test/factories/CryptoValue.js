"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const CryptoValue_1 = __importDefault(require("../../src/factories/CryptoValue"));
const CryptoValue_2 = __importDefault(require("../../src/lib/CryptoValue"));
describe('factories > CryptoValue', () => {
    const factory = CryptoValue_1.default();
    const fixtures = [
        { args: { device: {}, symbol: 'BTC' }, expected: { klass: CryptoValue_2.default } }
    ];
    fixtures.forEach(({ args, expected }) => {
        describe(`when call with ${JSON.stringify(args)}`, () => {
            const result = factory(args);
            it(`should return instance of "${expected.klass}"`, () => {
                chai_1.expect(result).to.be.instanceOf(expected.klass);
            });
        });
    });
});
//# sourceMappingURL=CryptoValue.js.map